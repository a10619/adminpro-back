const { response } = require('express');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');
const { ADMIN_ROLE } = require('../helpers/vars');



const getUsers = async (req, res = response) => {
    /**
     * Return_list users paginated
     */

    let { limit = 3, page = 0 } = req.query
    page = parseInt(page)
    limit = parseInt(limit)

    let total_pages = null
    let previous = null
    let next = null
    let next_page = page + limit

    // Get users paginated
    const [total, users] = await Promise.all([
        User.countDocuments({ status: true }),
        User.find()
        .skip(parseInt(page))
        .limit(parseInt(limit))
        .sort([['date', -1]])
    ])

    if (next_page < total) {
        next = `${process.env.URL_USER}?page=${next_page}`
    }
    if (page >= limit) {
        previous = `${process.env.URL_USER}?page=${page - limit}`
    } 

    if (page < total)  {
        
        let number_pages = total/limit
        if (Number.isInteger(number_pages)){
            total_pages = number_pages
        } else {
            total_pages = Math.round(number_pages + 0.5)
        }
    } else {
        total_pages = 1
    }


    res.json({
        total_pages,
        page,
        next,
        previous,
        total,
        users,
    })
}


const getUser = async (req, res = response) => {
    /**
     * Return user
     */
    const {id} = req.params

    // Get users paginated
    const user = await User.findById(id)
    res.json(user)
}


const postUser = async (req, res = response) => {
    const { password } = req.body
    const user = new User(req.body)

    // encriptar password
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(password, salt);

    // save user
    await user.save()

    const token = await generarJWT(user.id)
    res.json({ ok:true, user, token })
    
}

const putUser = async (req, res = response) => {
    const { id } = req.params

    let bodyReq = req.body
    const {email} = bodyReq

    
    const userById = await User.findById(id);
    
    if (id  === req.uid && userById !== ADMIN_ROLE){
        const { role, ...body} = bodyReq
        bodyReq = body
    }

    if (userById.email !== email) {
        const existEmail = await User.findOne({ email });

        if (existEmail) {
            return res.status(400).json({ 
                ok: false, msg: `Email ${ email } is already register!`
            })
        }
    }


    // update user
    const user = await User.findByIdAndUpdate(id, bodyReq, {new:true})

    res.json({ ok: true, user })
}

const deleteUser = async (req, res = response) => {
    const { id } = req.params

    await User.findByIdAndDelete(id)

    res.status(200).json({ok: true, msg:"Usuario borrado corrrectamente!"})
}



module.exports = {
    getUsers,
    getUser,
    postUser,
    putUser,
    deleteUser
}