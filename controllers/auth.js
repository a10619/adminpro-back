const bcrypt = require('bcryptjs');
const { response } = require('express');

const { generarJWT } = require('../helpers/jwt')
const { checkSideBarMenu } = require('../helpers/sidebar')
const { verifyGoogle } = require('../helpers/google-verify');

const User = require('../models/user');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const login = async (req, res = response) => {

    const { email, password } = req.body

    // Verify email
    const user = await User.findOne({ email })

    if (!user) {
        return res.status(400).json({ 
            ok: false, msg: "Contraseña/email incorrecto!"
        })

    }

    // verificar contraseña 
    const validatePass = bcrypt.compareSync(password, user.password)

    if (!validatePass || !user) {
        return res.status(400).json({
            ok: false,
            msg: "Contraseña/email incorrecto"
        })
    }
    
    // return sidebar acording ro role
    const sidebar = checkSideBarMenu(user.role)

    
    // GENERAR JWT TOKEN
    const token = await generarJWT(user.id)
    res.status(200).json({
        ok: true, token, user, sidebar
    })
}



/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const  signGoogle = async (req, res = response) => {
    
    try {
        const { token }  = req.body 
        
        const {email, name, picture } =  await verifyGoogle(token) // verificar que el token sea valido
        const userDb = await User.findOne({email})
        let user;

        if (!userDb) { // verificar que el usuario NO exista en db
            user = new User({
                name,
                email,
                img: picture,
                google: true,
            })
        } else {
            user = userDb
            user.google = true
        }
        // save user 
        await user.save()

         // GENERAR JWT TOKEN

        const tokenjwt = await generarJWT(user.id)

        const sidebar = checkSideBarMenu(user.role)

        res.status(200).json({
            ok: true, token:tokenjwt, user, sidebar
        })
        
    } catch (error) {
        console.log(error)
        res.status(400).json({ok:false, msg:"Token incorrecto"})
        
    }
}



/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
const rnvToken = async (req, res = response ) => {

    const uid = req.uid

    const token = await generarJWT(uid)
    const user = await User.findById(uid)

    const sidebar = checkSideBarMenu(user.role)

    // GENERAR JWT TOKEN
    res.status(200).json({
        ok: true, token, user, sidebar
    })
}



module.exports = {
    login, signGoogle, rnvToken
}