const { response } = require("express");
const User = require('../models/user');
const Medico = require('../models/medico');
const Hospital = require('../models/hospital');
const errorcode = require("../helpers/error-code");


const allowedCollections = [
    'users',
    'hospitales',
    'medicos'
]

const getAlls = async (req, res = response) => { 

    try {
        const { search } = req.params
        
        const regexp = new RegExp(search, 'i');
    
        const [ users, medicals, hospitals ] = await  Promise.all([
            User.find({
                $or: [
                    { 'name': regexp },
                    { 'email': regexp }
                ]
            }),
            Medico.find({ name: regexp }),
            Hospital.find({ name: regexp })
        ])  
        
        res.json({
            ok: true,
            users, 
            medicals, 
            hospitals
        })
        
    } catch (error) {
        errorcode(res, error)
    }


}

const getSearchByCollection = async (req, res = response) => {


    try {
        const { coleccion: collection, search } = req.params;
    
        if (!allowedCollections.includes(collection)) {
            return res.status(404).json({ 'msg': "not found collection, the collections allowed are: " + allowedCollections })
        }
        const regex = new RegExp(search, 'i')
    
        switch (collection) {
            case 'users':
                searchUser(regex, res)
                break;
    
            case 'hospitales':
                searchHospitales(regex, res)
                break;
            case 'medicos':
                searchMedicos(regex, res)
                break;
            default:
                res.status(404).json({ 'msg': "Se le olvido hacer la busqueda" })
        }
        
    } catch (error) {
        
    }


}

const searchUser = async (regex = '', res = response) => {

    const users = await User.find({
        $or: [{ name: regex }, { email: regex }],
        $and: [{ status: true }]
    });
    res.json({
        results: users
    })
}

const searchHospitales = async (regex = '', res = response) => {

    const hospitales= await Hospital.find({ name: regex, status:true })
                                    .populate('user', 'name img email')
    res.json({
        results: hospitales
    })
}

const searchMedicos = async (regex = '', res = response) => {

    const medicos = await Medico.find({ name: regex, status: true })
                                .populate('user', 'name img')
                                .populate('hospital', 'name')
    res.json({
        results: medicos
    })
}


module.exports = {
    getAlls,
    getSearchByCollection
}