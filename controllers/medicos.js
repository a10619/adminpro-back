const { response } = require("express")
const errorcode = require("../helpers/error-code")
const Medical = require('../models/medico')


const getMedicals = async(req, res = response) => {

    const { limite = 5, desde = 0 } = req.query

    const [total, medicals] = await Promise.all([
        Medical.countDocuments({ status: true }),
        Medical.find()
            .skip(parseInt(desde))
            .limit(parseInt(limite))
            .populate('user', 'name img')
            .populate('hospital', 'name img')
            .sort([['date', -1]])
    ])

    res.json({
        ok: true,
        total,
        medicals
    })
}



const getMedical = async(req, res = response) => {

    try{
        const id = req.params.id
        const medical = await Medical.findById(id)
    
    
        res.json({
            ok: true,
            medical
        })

    } catch(error){
        errorcode(res, error)    
    }

}

const postMedical = async(req, res = response) => {

    try {
        req.body.user = req.uid
        const medical = new Medical(req.body)
        await medical.save()

        res.json({
            ok: true,
            medico: medical
        })

        
    } catch (error) {
        errorcode(res, error)    
    }
}

const putMedical =  async (req, res = response) => {

    try {
        const uid = req.uid
        const id = req.params.id

        const changes = {
            ...req.body,
            user:uid
        }

        const medical = await Medical.findByIdAndUpdate(id, changes, {new: true})

        res.json({ok:true, medical})
        
    } catch (error) {
        errorcode(res, error)    
    }
}

const deleteMedico = async (req, res = response) => {

    try {
        const id = req.params.id
        const medical  = await Medical.findByIdAndDelete(id)

        res.json({ok:true, medico: medical})
        
    } catch (error) {
        errorcode(res, error)
    }
}



module.exports = {
    getMedicals,
    getMedical,
    postMedical,
    putMedical,
    deleteMedico
}