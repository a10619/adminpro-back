const { Router } = require('express')
const { uploadFile, returnImg } = require('../controllers/uploads')
const expressUpload = require('express-fileupload');
const { check } = require('express-validator')
const { validateFields } = require('../middlewares/validate-fields')
const { validarJWT } = require('../middlewares/validar-jwt')


const router = new Router();

router.use(expressUpload());

router.put('/:coleccion/:id', [
    validarJWT,
    check('id', "Mongo id invalido").isMongoId(),
    validateFields
], uploadFile )


router.get('/:coleccion/:photo', returnImg)


module.exports = router