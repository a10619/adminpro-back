const { Router } = require('express')
const { getMedicals,getMedical, postMedical, putMedical, deleteMedico } = require('../controllers/medicos')
const { check } = require('express-validator')
const { validateFields } = require('../middlewares/validate-fields')
const { existHospitalById, existMedicalById } = require('../helpers/db-validators')
const { validarJWT } = require('../middlewares/validar-jwt')

const router = new Router();

router.get('/', [
    validarJWT,
    validateFields
], getMedicals)

router.get('/:id', [
    validarJWT,
    check('id', 'Mongo id invalido').isMongoId(),
    check('id').custom(existMedicalById),
    validateFields
], getMedical)


router.post('/', [
    validarJWT,
    check('name', 'El nombre del medico es requerido').notEmpty(),
    check('hospital', 'El id del hospital es requerido').notEmpty(),
    check('hospital', 'El id del hospital no es un mongo id valido').isMongoId(),
    check('hospital').custom(existHospitalById),
    validateFields
], postMedical)

router.put('/:id', [
    validarJWT,
    check('id', 'Mongo id invalido').isMongoId(),
    check('id').custom(existMedicalById),
    validateFields
], putMedical)

router.delete('/:id', [ 
    validarJWT,
    check('id', 'Mongo id invalido').isMongoId(),
    check('id').custom(existMedicalById),
    validateFields
], deleteMedico)


module.exports = router