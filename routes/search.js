const { Router } = require('express')

const { validarJWT } = require('../middlewares/validar-jwt')
const { getAlls, getSearchByCollection   } = require('../controllers/search')

const router = new Router();

router.get('/all/:search', [validarJWT], getAlls)
router.get('/:coleccion/:search',[validarJWT], getSearchByCollection)

module.exports = router