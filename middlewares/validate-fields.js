const { response } = require("express")
const { validationResult } = require("express-validator")
const { ADMIN_ROLE } = require("../helpers/vars")
const User = require("../models/user")


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
const validateFields = (req, res = response, next) => {
    const errors = validationResult( req )

    if ( !errors.isEmpty() ){
        return res.status(400).json({
            ok: false,
            errors: errors.mapped()
        })
    }
    next();
}



/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
const userIsAdmin =  async(req, res = response, next) => {
    const uid = req.uid

    const user = await User.findById(uid)

    if (user.role !== ADMIN_ROLE){
        return res.status(400).json({
            ok: false,
            errors: "You don't have permissions for this" 
        })
    }
    next();
}


const userIsAdminOrIsSame =  async(req, res = response, next) => {
    const uid = req.uid
    const { id } = req.params

    const user = await User.findById(uid)
    if (user.role === ADMIN_ROLE || uid === id) { // uid === id ? is the same user
        next();
    } else {
        return res.status(400).json({
            ok: false,
            errors: "You don't have permissions for this" 
        })
    }
}


module.exports = {
    validateFields,
    userIsAdmin,
    userIsAdminOrIsSame
}