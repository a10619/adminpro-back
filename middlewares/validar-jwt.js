const { response, request } = require('express')
const jwt = require('jsonwebtoken')
const  User = require('../models/user')

const validarJWT =  async(req, res = response, next) => {

    // leer el header and token
    const token = req.header('x-token')

    if (!token) return res.status(404).json({msg:'No is token in the request'})
    
    try {
        // verificar si el token es validox,Retorna el uid del usuario
        const { uid } = jwt.verify(token, process.env.PRIVATEKEY)
        
        // set user request
        const user = await User.findById( uid )

        // verificar si le usuario existe en la BD
        if (!user) {
            return res.status(401).json({msg:'Not valid token'})
        }

        // verificar si el usuario esta activo
        if (!user.status) {
            return res.status(401).json({msg:'Not valid token'})
        }

        
        // set user in current app
        req.uid = uid
        next()

    } catch (e) {
        console.log(e)
        res.status(401).json({
            ok:false,
            msg:'Not valid token'
        })  
    }
    
}


module.exports = {
    validarJWT
}