const path = require('path')
const { v4: uuidv4 } = require('uuid');
const fs = require('fs')

const Medical = require("../models/medico");
const User = require("../models/user");
const Hospital = require('../models/hospital');
const validExtensions = ['jpg', 'jpeg', 'png', 'gif', 'webp']

const subirArchivo = (archivo, folder = '' ) => {


    return new Promise((resolve, reject) => {

        const nameCortado = archivo.name.split('.')
        const extension = nameCortado[nameCortado.length - 1]

        if (!validExtensions.includes(extension)) {
            resolve({ ok: false, msg:`only is allowed images with follwed extensions ${validExtensions}`})
        }
        // filename
        const nameTemp = uuidv4() + '.' + extension
        
        const uploadPath = path.join(__dirname + '/../uploads/',folder)
        const fullPath = `${uploadPath}/${nameTemp}`

        // if not exist folder, create it
        if (!fs.existsSync(uploadPath)){
            fs.mkdirSync(uploadPath, { recursive: true });
        }
        
        archivo.mv(fullPath, (err) => {
            if (err) {
                reject(err)
        } else {
            resolve({ok: true, data: nameTemp})
            }
        });
    })
}


const verifyIfExistBd = (coleccion, id) => {
    return new Promise(async (resolve, reject) => {

        let model = null;
        switch (coleccion) {
            case 'users':
                model = await User.findById(id)

                if (!model) {
                    reject({ msg: `No exist un user by the id  ${id}` })
                }
                break;

            case 'medicals':
                model = await Medical.findById(id)
                if (!model) {
                    reject({ msg: `No exist un medical by the id  ${id}` })
                }
                break;
            case 'hospitals':
                model = await Hospital.findById(id)
                if (!model) {
                    reject({ msg: `No exist hospital by the id ${id}` })
                }
                break;
        }
        resolve(model)
    })

}


const updateFile = async (coleccion, fileUpload, model) => {
    
    const pathLast = path.join(__dirname + `/../uploads/${coleccion}/${model.img}`)
    // preguntar si la imagen ya esta cargada en el servidor
    if (fs.existsSync(pathLast)) {
        // delete img   
        fs.unlinkSync(pathLast)

    }
    model.img = fileUpload
    await model.save();
}



module.exports = {
    subirArchivo,
    verifyIfExistBd,
    updateFile
    

}