const Hospital = require('../models/hospital');
const Medical = require('../models/medico');
const User = require('../models/user');
const { ADMIN_ROLE, USER_ROLE } = require('./vars');

const existEmail = async (email = '' )  => {

    const existEmail = await User.findOne({ email });

    if (existEmail) {
        throw new Error(`El correo ${email} ya se encuentra registrado!`)
    }
}

const existUserById = async (id = '') => {
    const existUserById = await User.findById(id)

    if (!existUserById) {
        throw new Error('No existe un usuario con ese id!')
    }
}

const existMedicalById = async (id = '') => {
    const existMedicalById = await Medical.findById(id)

    if (!existMedicalById) {
        throw new Error('That medical not exist!')
    }
}

const existHospitalById = async id => {
    const existHospitalById = await Hospital.findById(id)

    if (!existHospitalById) {
        throw new Error('No existe un hospital con ese id!')
    }

}


/**
 * 
 * @param {*} role 
 */
const roleIsValid = async role => {
    if(role){
        if (role !== ADMIN_ROLE && role !== USER_ROLE) {
            throw new Error('The role no is correct!')
        }
    }

}



module.exports = {
    existEmail,
    existUserById,
    existHospitalById,
    existMedicalById,
    roleIsValid
}