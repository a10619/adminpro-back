const { response } = require("express")


const errorcode = (res = response, err) => {
    console.log(err)
    return res.status(401).json({
        ok: false,
        msg: "Opsss.. there was an error, please contact the administrator"
    })
}
module.exports = errorcode