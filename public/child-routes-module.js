(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["child-routes-module"],{

/***/ "E5tf":
/*!**********************************************!*\
  !*** ./src/app/pages/child-routes.module.ts ***!
  \**********************************************/
/*! exports provided: ChildRoutesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChildRoutesModule", function() { return ChildRoutesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _account_settings_account_settings_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./account-settings/account-settings.component */ "SxNo");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "U5Cf");
/* harmony import */ var _grafica1_grafica1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./grafica1/grafica1.component */ "XHMk");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile/profile.component */ "Y5Lh");
/* harmony import */ var _progress_progress_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./progress/progress.component */ "EsRH");
/* harmony import */ var _promises_promises_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./promises/promises.component */ "VAHl");
/* harmony import */ var _rxjs_rxjs_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./rxjs/rxjs.component */ "IHpz");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./search/search.component */ "YQc0");












const childRoutes = [
    { path: '', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"], data: { title: 'Dashboard' } },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__["ProfileComponent"], data: { title: 'My Profile' } },
    { path: 'progress', component: _progress_progress_component__WEBPACK_IMPORTED_MODULE_6__["ProgressComponent"], data: { title: 'ProgressBar' } },
    { path: 'grafica1', component: _grafica1_grafica1_component__WEBPACK_IMPORTED_MODULE_4__["Grafica1Component"], data: { title: 'Grafica 1' } },
    { path: 'account-settings', component: _account_settings_account_settings_component__WEBPACK_IMPORTED_MODULE_2__["AccountSettingsComponent"], data: { title: 'Settings theme' } },
    { path: 'promises', component: _promises_promises_component__WEBPACK_IMPORTED_MODULE_7__["PromisesComponent"], data: { title: 'Promises' } },
    { path: 'rxjs', component: _rxjs_rxjs_component__WEBPACK_IMPORTED_MODULE_8__["RxjsComponent"], data: { title: 'Rxjs' } },
    // mantenimientos
    {
        path: '',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./maintenances/maintenances.module */ "/Sr2")).then(m => m.MaintenancesModule)
    },
    { path: 'search/:value', component: _search_search_component__WEBPACK_IMPORTED_MODULE_9__["SearchComponent"], data: { title: 'Search results' } },
];
class ChildRoutesModule {
}
ChildRoutesModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: ChildRoutesModule });
ChildRoutesModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function ChildRoutesModule_Factory(t) { return new (t || ChildRoutesModule)(); }, imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(childRoutes)
        ], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ChildRoutesModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChildRoutesModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(childRoutes)
                ],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=child-routes-module.js.map