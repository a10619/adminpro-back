const { Schema, model } = require('mongoose')

const MedicoSchema = Schema({
    name: {
        type: String,
        required: true
    },
    img: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    hospital: {
        type: Schema.Types.ObjectId,
        ref: 'Hospital',
        required: true
    },
    status: {
        type: Boolean,
        default: true
    },
    date: {
        type: Date,
        default: new Date()
    }
});

MedicoSchema.method('toJSON', function () {
    const { __v, _id, ...obj } = this.toObject();

    obj.id = _id

    return obj
})

module.exports = model('Medico', MedicoSchema)